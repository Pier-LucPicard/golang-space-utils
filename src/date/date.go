package date

import (
	"strings";
	"strconv";
)

type Date struct {
	year int
	month int
	day int
}

var months = []int{31,28,31,30,31,30,31,31,30,31,30,31};



func CreateDate(year, month, day int)(d Date){
	d = Date{year, month, day}
	return
}

func (d Date) GetStarDate() (string) {
	// calculate left part of the stardate
	leftpart:=d.day
	for i:= 0; i < d.month; i++{
		leftpart += months[i]
	}

	date := []string{strconv.Itoa(d.year), strconv.Itoa(leftpart)}
	return strings.Join(date, ".")
}