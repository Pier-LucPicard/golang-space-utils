package person

import (
	"../date"
)

type Person struct {
	name string
	age int
	birth_date date.Date
}


func createPerson()(p Person){
	p = Person{"tom", 10, date.CreateDate(1995,10,10)}
	return 
}