package src

import(
	//"fmt"
	"../../fileUtil"
	"../../delayText"
	"../../commands"
	"./GameController"
)

func Start(){
	gameControllerCommanger:=commands.CreateCommander()

	gameControllerCommanger.RegisterCommand("Exit", GameController.Exit)
	gameControllerCommanger.RegisterCommand("Start", GameController.Start)

	gameController:=delayText.CreateActor(5 ,gameControllerCommanger);
	titleScreen := fileUtil.LoadActorScript("src/Game/files/Title_Screen.txt");

	///fmt.Print(titleScreen)
	gameController.ActScript(titleScreen)


}