package commands

import (
	"fmt"
	"reflect"
	"strings"
)
type Commander struct {
	commands map[string] func(args ...string) int
}

func CreateCommander()(c Commander){
	
	c=Commander{ make(map[string] func(args ...string) int)}
	return
}


func (c Commander) RegisterCommand(name string, f func(args ...string) int){
	c.commands[name] = f
}


func (c Commander) RunHelp(name string, args ...string) int{
	if _ , ok := c.commands[name[0:len(name)-1]]; !ok  {
		fmt.Print("Not Found.\nThe Accepted commands are: ")
		keys := reflect.ValueOf(c.commands).MapKeys()
		strkeys := make([]string, len(keys))
		for i := 0; i < len(keys); i++ {
			strkeys[i] = keys[i].String()
		}
		fmt.Print(strings.Join(strkeys, ", "))
		fmt.Print("\n")

		return 0
	}
	return c.commands[name[0:len(name)-1]](args...)

}
func (c Commander) Run(name string, args ...string) int{
	if _ , ok := c.commands[name[0:len(name)-1]]; !ok  {
		fmt.Print("Not Found.\n")

		return 0
	}
	return c.commands[name[0:len(name)-1]](args...)

}
