package language

import (
	"fmt"
	"regexp"
	"encoding/json"
)

type SyntaxerRule struct {
	Rule string
	Replacement string
}


func GenerateRule(rulesJsonBytes []byte)(syntaxrule []SyntaxerRule){
 	err := json.Unmarshal(rulesJsonBytes,&syntaxrule)
		if err != nil {
		fmt.Println("error:", err)
	}
	return
}

func Write(syntaxrule []SyntaxerRule, sentence string)(e string){
	e=sentence
	for i:=0; i<len(syntaxrule); i++ {
		var validID = regexp.MustCompile(syntaxrule[i].Rule)
		e = validID.ReplaceAllString(e, syntaxrule[i].Replacement)
	}
	return
}