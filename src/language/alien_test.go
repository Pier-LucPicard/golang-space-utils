package language

import (
	"testing"
	"strings"
)

func TestWriteWithoutRule(t *testing.T) {
	originalString := "Original String"
	var rules []SyntaxerRule = []SyntaxerRule{}
	actual := Write(rules, originalString)

	if strings.Compare(actual, originalString) != 0  {
	  t.Errorf("Expected the string \"%s\" to equal \"%s\"", actual, originalString)
	}
}

func TestWriteWithRule(t *testing.T) {
	originalString := "Original String"
	expectedString := "Oyiginayyb Stying"
	var rules []SyntaxerRule = []SyntaxerRule{SyntaxerRule{`[a]`,`ar`},SyntaxerRule{`[l]`,`eb`}, SyntaxerRule{`[re]`,`y`}};
	actual := Write(rules, originalString)
  
	if strings.Compare(actual, expectedString) != 0  {
	  t.Errorf("Expected the string \"%s\" to equal \"%s\"", actual, expectedString)
	}
}