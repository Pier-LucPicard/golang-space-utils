package delayText

import (
	"fmt"
	"time"
	"bufio"
	"os"
	"math/rand"
	"strings"
	"strconv"
	"../commands"
)


type Actor struct {
	max_delay int 
	variable_type  map[string] string
	string_variables map[string] string
	int_variables map[string] float64
	commander commands.Commander
}

func CreateActor(delay int, c commands.Commander)(a Actor){
	a=Actor{delay, make(map[string]string),make(map[string]string), make(map[string]float64), c}
	return
}

func (a Actor) ActScript(script string){
	sentences := strings.Split(script, "\n")
	
	for i:=0; i<len(sentences); i++{

		if strings.Split(sentences[i], " ")[0] == "#input"{
			a.actInput(sentences[i])
		}else if (strings.Split(sentences[i], " ")[0] == "#pause") {
			num, _ := strconv.Atoi(strings.Split(sentences[i], " ")[1])
			a.PauseMS(num)
		}else if (strings.Split(sentences[i], " ")[0] == "#run") {
			a.actRun(sentences[i])
		}else if (strings.Split(sentences[i], " ")[0] == "#runHelp") {
			a.actRunHelp(sentences[i])
		}else{
			words := strings.Split(sentences[i], " ")
	
			for j:=0; j<len(words) ;j++{
		
				if words[j]  == "" {
					
				} else if rune(words[j][0]) == '*' && rune(words[j][len(words[j])-1]) == '*'{
					a.printVariable(words[j][1:len(words[j])-1])
				}else{
					a.Print(words[j], true)
				}
				a.Print(" ", true)
			}
			
			a.Print("\n",true)
		}
	}
}


func (a Actor) actRunHelp(sentence string){
	for good:=0;good != 1; {
	words := strings.Split(a.askCommand(), " ");

		if(len(words) > 1){
			good = a.commander.RunHelp(words[0], words[1:len(words)-1]...)
		}else{
			good = a.commander.RunHelp(words[0])
		}
	}


}

func (a Actor) actRun(sentence string){
	for good:=0;good != 1; {
	words := strings.Split(a.askCommand(), " ");

	

		if(len(words) > 1){
			good = a.commander.Run(words[0], words[1:len(words)-1]...)
		}else{
			good = a.commander.Run(words[0])
		}
	}


}

func (a Actor) actInput(sentences string){
	words := strings.Split(sentences, " ")
	a.AskInput(words[1], words[2])
}

func (a Actor) askNumberInput(mapName string) {
	var f float64
	fmt.Scanf("%f", &f)
	a.variable_type[mapName] = "number"
	a.int_variables[mapName] =f
}

func (a Actor) askStringInput(mapName string) {
	reader := bufio.NewReader(os.Stdin)
	a.variable_type[mapName] = "string"
	var s string;
	s, _=reader.ReadString('\n')
	a.string_variables[mapName]= s[0:(len(s)-1)]
}

func (a Actor) askCommand() (s string){
	reader := bufio.NewReader(os.Stdin)
	s,  _=reader.ReadString('\n')
	return 
}

func (a Actor) AskInput(mapName, varType string) {
	switch varType {
	case "string":
		a.askStringInput(mapName)
	case "number":
		a.askNumberInput(mapName)
	}
	return
}

func (a Actor) printVariable(mapName string) {
	switch a.variable_type[mapName]{
	case "string":
		a.Print(a.string_variables[mapName], true)
	case "number":
		a.Print(strconv.FormatFloat(a.int_variables[mapName],'f',-1,64), true)
	}
}

func (a Actor) Print(sentence string, hasPause bool){
	rand.Seed(time.Now().UnixNano())


	for i:=0; i<len(sentence); i++{
		fmt.Printf("%c",sentence[i])
		if hasPause{
			a.PauseMS(rand.Intn(a.max_delay) )
		}
		
	}
}

func (a Actor) PauseMS(duration int){
	time.Sleep(time.Duration(duration) *time.Millisecond)
}