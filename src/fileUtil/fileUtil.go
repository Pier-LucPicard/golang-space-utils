package fileUtil

import (
	"fmt"
	"log"
	"io/ioutil"
)

func LoadActorScript(fileName string)(content string){
	fmt.Println("Loading Script:", fileName)
	bytes, err:= ioutil.ReadFile(fileName)


	if err != nil {
		log.Fatalln("failed to open config:", err)
	}
	content = string([]byte(bytes))
	return 
}

func LoadLanguage(fileName string) (bytes []byte){

	fmt.Println("Loading Language:", fileName)
	bytes, err:= ioutil.ReadFile(fileName)

	if err != nil {
		log.Fatalln("failed to open config:", err)
	}
	return 
}

