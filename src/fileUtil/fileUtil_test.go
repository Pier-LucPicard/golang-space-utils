package fileUtil

import (
	"testing"
	"reflect"
)

func TestLoadLanguage(t *testing.T) {

	b := []byte(LoadLanguage("../../files/erjeb.json"))
	expected := []byte(`[{"Rule":"[a]","Replacement":"ar"},{"Rule":"[l]","Replacement":"eb"},{"Rule":"[re]","Replacement":"y"}]`)
	if reflect.DeepEqual(b, expected){
		t.Errorf("Expected the string \"%s\" to equal \"%s\"", b, expected)
	}
}